import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { TaskService } from '../services/task.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  profileForm = new FormGroup({
    name: new FormControl(''),
    status: new FormControl('')
  });
  constructor(
    private fb: FormBuilder,
    private taskService: TaskService
  ) { }

  taskForm = this.fb.group({
    name: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15)
    ])],
    status: ['', Validators.required]
  });

  submit() : void {
    this.taskService.add(
      this.taskForm.value
    );
  }

  ngOnInit() {
  }

}
